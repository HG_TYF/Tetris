package com.mochizuki.tetris;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Timer;
import com.mochizuki.tetris.Bricks.AbsBrick;
import com.mochizuki.tetris.Bricks.JshapeBrick;
import com.mochizuki.tetris.Bricks.LongBrick;
import com.mochizuki.tetris.Bricks.LshapeBrick;
import com.mochizuki.tetris.Bricks.SquareBrick;
import com.mochizuki.tetris.Bricks.SshapeBrick;
import com.mochizuki.tetris.Bricks.TshapeBrick;
import com.mochizuki.tetris.Bricks.ZshapeBrick;

public class MainScreen implements Screen {
	class GameOverDialog extends Dialog {

		private TextureRegionDrawable mDrawableBtnUp;
		private TextureRegionDrawable mDrawableBtnDown;

		private TextButton.TextButtonStyle mBtnStyle;
		private Texture m_buttonTexture;

		private Label.LabelStyle mLabelStyle;
		private Label mScore;

		public GameOverDialog(String title, WindowStyle windowStyle,
				BitmapFont font) {
			super(title, windowStyle);
			// TODO Auto-generated constructor stub
			m_buttonTexture = new Texture(
					Gdx.files.internal("buttons/button228x31.png"));
			mDrawableBtnUp = new TextureRegionDrawable(new TextureRegion(
					m_buttonTexture, 0, 0, 107, 31));
			mDrawableBtnDown = new TextureRegionDrawable(new TextureRegion(
					m_buttonTexture, 121, 0, 107, 31));

			mBtnStyle = new TextButton.TextButtonStyle(mDrawableBtnUp,
					mDrawableBtnDown, mDrawableBtnUp);
			mLabelStyle = new Label.LabelStyle();

			mBtnStyle.font = font;
			mLabelStyle.font = font;
			mScore = new Label("0", mLabelStyle);
			mScore.setX(100);
			mScore.setY(262);
			addActor(mScore);
			button(m_Context.getString(android.R.string.ok), 1, mBtnStyle);

		}

		@Override
		protected void result(Object object) {
			// TODO Auto-generated method stub
			((MainActivity) m_Context).toMenu();
			super.result(object);
		}

		public void setScore(int score) {
			mScore.setText(String.valueOf(score));
		}
	}

	// 水平连续移动任务
	private Timer.Task m_HorizTimertask = new Timer.Task() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			switch (m_HorizMoveDirect) {
			case H_LEFT:
				MoveLeft();
				break;
			case H_RIGHT:
				MoveRight();
				break;
			}
		}
	};

	// 方块下落任务
	private Timer.Task m_MainTask = new Timer.Task() {
		@Override
		public void run() {
			// TODO Auto-generated method stub
			if (m_IsGameOver)
				return;
			if (m_Board.canDown(m_Brick)) {
				Down();
			} else {
				m_SoundDown.play();
				if (m_Brick.getCoordLine() == 0) {
					m_IsGameOver = true;
					// Label.LabelStyle mLabelStyle = new
					// Label.LabelStyle();
					//
					// mLabelStyle.font = m_Font;
					// m_GameOverdlg.text(m_context.getString(
					// R.string.game_yourscore, m_Score), mLabelStyle);
					m_Music.stop();
					m_SoundLose.play();
					m_GameOverdlg.setScore(m_Score);
					m_GameOverdlg.show(m_Stage);
					return;
				}
				// m_NeedNewBrick = true;
				ShowNextBrick();
			}

		}
	};

	private float POWER_X, POWER_Y;
	private float SCREEN_WIDTH, SCREEN_HEIGHT;

	private final float DEFAULT_SPEED = 0.8f;
	private final float ACC_SPEED = 0.08f;

	private final float PICE_WIDTH = 36f;

	Context m_Context;
	private Stage m_Stage;
	private Board m_Board;
	private Texture m_Texter;
	private Image m_Background;

	private AbsBrick m_Brick;
	private AbsBrick m_NextBrick;

	private Button m_LeftButton;
	private Button m_RightButton;
	private Button m_UpButton;
	private Button m_DownButton;
	private Button m_PauseButton;

	private boolean m_IsGameOver;
	private boolean m_Pause;

	private Timer m_MainTimer;
	private int m_Score;

	private Label m_LabelScore;
	//private Label m_LabelFPS;
	private BitmapFont m_Font;

	private Music m_Music;
	private Sound m_SoundButton;
	private Sound m_SoundRotation;
	private Sound m_SoundDown;
	private Sound m_SoundLose;
	private Sound[] m_SoundDelete;

	// private GameThread m_GameThread;
	private float m_GameSpeed;
	private int m_HorizMoveDirect;
	private GameOverDialog m_GameOverdlg;

	private PauseDialog m_PauseDialog;

	private boolean m_NewGame;

	public MainScreen(Context context) {
		// TODO Auto-generated constructor stub
		m_Context = context;
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		m_Texter.dispose();
		m_Board.dispose();
		m_Brick.dispose();
		m_Font.dispose();
		m_Stage.dispose();

		m_Music.dispose();
		m_SoundButton.dispose();
		m_SoundRotation.dispose();
		m_SoundDown.dispose();
		for (int i = 0; i < 4; i++)
			m_SoundDelete[i].dispose();

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		Log.i(getClass().getSimpleName(), "============Pause...==========");
		SaveState();
		m_MainTimer.stop();
		m_MainTimer.clear();
		m_IsGameOver = true;
	}

	private final int H_STATIC = 0;
	private final int H_LEFT = 1;
	private final int H_RIGHT = 2;

	@Override
	public void render(float arg0) {
		// TODO Auto-generated method stub
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		Gdx.gl.glClearColor(0f, 0f, 0f, 0f);

		m_LabelScore.setText(String.valueOf(m_Score));
		//m_LabelFPS.setText("FPS:" + Gdx.graphics.getFramesPerSecond());
		m_Stage.act(Gdx.graphics.getDeltaTime());
		m_Stage.draw();

	}

	@Override
	public void resize(int arg0, int arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		Log.i(getClass().getSimpleName(), "============resume...==========");
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		Log.i(getClass().getSimpleName(), "============show...==========");

		SCREEN_WIDTH = Gdx.graphics.getWidth();
		SCREEN_HEIGHT = Gdx.graphics.getHeight();
		POWER_X = SCREEN_WIDTH / 640f;
		POWER_Y = SCREEN_HEIGHT / 960f;

		m_Font = new BitmapFont(Gdx.files.internal("fonts/chnfont.fnt"), false);
		m_Font.setScale(POWER_X);

		m_Stage = new Stage(SCREEN_WIDTH, SCREEN_HEIGHT, false);

		m_Texter = new Texture(Gdx.files.internal("background/main312x397.png"));
		m_Background = new Image(new TextureRegion(m_Texter, 320, 480));
		m_Background.setWidth(SCREEN_WIDTH);
		m_Background.setHeight(SCREEN_HEIGHT);
		m_Stage.addActor(m_Background);

		m_Board = new Board(PICE_WIDTH * POWER_X, PICE_WIDTH * POWER_Y);
		m_Board.setX(POWER_X * 34f);
		m_Board.setY(POWER_Y * 182f);

		m_Stage.addActor(m_Board);

		if (m_MainTimer == null)
			m_MainTimer = new Timer();
		if (!m_MainTask.isScheduled())
			m_MainTimer.scheduleTask(m_MainTask, DEFAULT_SPEED, DEFAULT_SPEED);

		// 显示得分的label
		LabelStyle lbs = new LabelStyle();
		lbs.font = m_Font;
		m_LabelScore = new Label(m_Context.getString(R.string.game_score, 0),
				lbs);
		m_LabelScore.setX(m_Board.getWidth() + 114f * POWER_X);
		m_LabelScore.setY(374f * POWER_Y + m_Board.getY());

//		m_LabelFPS = new Label("", lbs);
//		m_LabelFPS.setY(SCREEN_HEIGHT - 100f * POWER_Y);

		m_Stage.addActor(m_LabelScore);
//		m_Stage.addActor(m_LabelFPS);
		createButtons();
		CreateDialog();

		// 暂停对话框
		Window.WindowStyle style = new Window.WindowStyle();

		style.titleFont = m_Font;
		style.background = new TextureRegionDrawable(new TextureRegion(
				new Texture(Gdx.files.internal("background/pause269x383.png")),
				0, 0, 269, 383));
		m_PauseDialog = new PauseDialog("", style, m_Context, m_GameControl);
		m_PauseDialog.setWidth(320f * POWER_X);
		m_PauseDialog.setHeight(480f * POWER_Y);
		m_PauseDialog.setX((SCREEN_WIDTH - m_PauseDialog.getWidth()) / 2);
		m_PauseDialog.setY((SCREEN_HEIGHT - m_PauseDialog.getHeight()) / 2);

		initSounds();

		Gdx.input.setInputProcessor(m_Stage);
		if (m_NewGame)
			StartNewGame();
		else
			this.ResumeGame();
	}

	private PauseDialog.GameControl m_GameControl = new PauseDialog.GameControl() {

		@Override
		public void Resume() {
			// TODO Auto-generated method stub
			m_PauseDialog.remove();
			m_Pause = false;
			m_MainTimer.start();
			m_Music.play();

		}

		@Override
		public void NewGame() {
			// TODO Auto-generated method stub
			m_PauseDialog.remove();
			m_Pause = false;
			StartNewGame();
		}

		@Override
		public void Exit() {
			// TODO Auto-generated method stub
			((MainActivity) m_Context).finish();
		}
	};

	private void createButtons() {

		ClickListener cllistener = new ClickListener() {

			Timer time = new Timer();

			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				// TODO Auto-generated method stub
				// 暂停或结束时不接受输入事件
				if (m_IsGameOver || m_Pause)
					return super.touchDown(event, x, y, pointer, button);
				if (event.getListenerActor() == m_DownButton) {
					// Playing();
					m_GameSpeed = ACC_SPEED;
					m_MainTimer.stop();
					m_MainTimer.clear();
					m_MainTimer.scheduleTask(m_MainTask, ACC_SPEED, ACC_SPEED);
					m_MainTimer.start();

				}
				if (event.getListenerActor() == m_LeftButton) {
					MoveLeft();
					m_HorizMoveDirect = H_LEFT;
					time.scheduleTask(m_HorizTimertask, 0.5f, 0.08f);
					time.start();
				}
				if (event.getListenerActor() == m_RightButton) {
					MoveRight();
					m_HorizMoveDirect = H_RIGHT;
					time.scheduleTask(m_HorizTimertask, 0.5f, 0.08f);
					time.start();
				}
				if (event.getListenerActor() == m_UpButton) {
					Rotation();

				}
				if (event.getListenerActor() == m_PauseButton) {
					m_Pause = true;
					m_MainTimer.stop();
					m_Music.pause();
					m_Stage.addActor(m_PauseDialog);

				}
				return super.touchDown(event, x, y, pointer, button);
			}

			@Override
			public void touchUp(InputEvent arg0, float arg1, float arg2,
					int arg3, int arg4) {
				// TODO Auto-generated method stub
				// if (m_IsGameOver || m_Pause)
				// return;
				if (arg0.getListenerActor() == m_DownButton) {
					m_GameSpeed = DEFAULT_SPEED;

					m_MainTimer.stop();
					m_MainTimer.clear();
					m_MainTimer.scheduleTask(m_MainTask, DEFAULT_SPEED,
							DEFAULT_SPEED);
					m_MainTimer.start();

				}
				if (arg0.getListenerActor() == m_LeftButton
						|| arg0.getListenerActor() == m_RightButton) {
					m_HorizMoveDirect = H_STATIC;
					time.stop();
					time.clear();
				}

				super.touchUp(arg0, arg1, arg2, arg3, arg4);
			}

		};
		float width = 80f * POWER_X, height = 96f * POWER_Y;
		ButtonStyle btnst = new ButtonStyle();
		Texture tmpTexture = new Texture(
				Gdx.files.internal("buttons/down160x96.png"));
		btnst.up = new TextureRegionDrawable(new TextureRegion(tmpTexture, 0,
				0, 80, 96));
		btnst.down = new TextureRegionDrawable(new TextureRegion(tmpTexture,
				80, 0, 80, 96));
		m_DownButton = new Button(btnst);

		btnst = new ButtonStyle();
		tmpTexture = new Texture(Gdx.files.internal("buttons/left160x96.png"));
		btnst.up = new TextureRegionDrawable(new TextureRegion(tmpTexture, 0,
				0, 80, 96));
		btnst.down = new TextureRegionDrawable(new TextureRegion(tmpTexture,
				80, 0, 80, 96));
		m_LeftButton = new Button(btnst);

		btnst = new ButtonStyle();
		tmpTexture = new Texture(Gdx.files.internal("buttons/right160x96.png"));
		btnst.up = new TextureRegionDrawable(new TextureRegion(tmpTexture, 0,
				0, 80, 96));
		btnst.down = new TextureRegionDrawable(new TextureRegion(tmpTexture,
				80, 0, 80, 96));
		m_RightButton = new Button(btnst);

		btnst = new ButtonStyle();
		tmpTexture = new Texture(Gdx.files.internal("buttons/rotate160x96.png"));
		btnst.up = new TextureRegionDrawable(new TextureRegion(tmpTexture, 0,
				0, 80, 96));
		btnst.down = new TextureRegionDrawable(new TextureRegion(tmpTexture,
				80, 0, 80, 96));
		m_UpButton = new Button(btnst);

		btnst = new ButtonStyle();
		Texture texture = new Texture(Gdx.files.internal("buttons/pause_E.png"));

		btnst.up = new TextureRegionDrawable(new TextureRegion(texture, 0, 0,
				78, 29));
		btnst.down = new TextureRegionDrawable(new TextureRegion(texture, 78,
				0, 78, 29));
		m_PauseButton = new Button(btnst);

		m_DownButton.addListener(cllistener);
		m_LeftButton.addListener(cllistener);
		m_RightButton.addListener(cllistener);
		m_UpButton.addListener(cllistener);
		m_PauseButton.addListener(cllistener);

		m_LeftButton.setWidth(width);
		m_LeftButton.setHeight(height);
		m_LeftButton.setY(height * 0.3f);
		m_LeftButton.setX(10f * POWER_X);

		m_UpButton.setWidth(width);
		m_UpButton.setHeight(height);
		m_UpButton.setX(m_LeftButton.getX() + width + 10f * POWER_X);
		m_UpButton.setY(m_LeftButton.getY());

		m_RightButton.setWidth(width);
		m_RightButton.setHeight(height);
		m_RightButton.setY(m_LeftButton.getY());
		m_RightButton.setX(m_UpButton.getX() + width + 10f * POWER_X);

		m_DownButton.setWidth(width);
		m_DownButton.setHeight(height);
		m_DownButton.setX(m_RightButton.getX() + width + 10f * POWER_X);
		m_DownButton.setY(m_LeftButton.getY());

		m_PauseButton.setWidth(156f * POWER_X);
		m_PauseButton.setHeight(58f * POWER_Y);
		m_PauseButton.setX(SCREEN_WIDTH - m_PauseButton.getWidth() - 20f
				* POWER_X);
		m_PauseButton.setY(320f * POWER_Y);

		m_Stage.addActor(m_LeftButton);
		m_Stage.addActor(m_RightButton);
		m_Stage.addActor(m_UpButton);
		m_Stage.addActor(m_DownButton);
		m_Stage.addActor(m_PauseButton);
	}

	private AbsBrick makeNewTetris() {
		AbsBrick result = null;

		int type = (int) Math.round(Math.random() * 6);
		switch (type) {
		case 0:
			result = new TshapeBrick();
			break;
		case 1:
			result = new LongBrick();
			break;
		case 2:
			result = new SquareBrick();
			break;
		case 3:
			result = new LshapeBrick();
			break;
		case 4:
			result = new JshapeBrick();
			break;
		case 5:
			result = new ZshapeBrick();
			break;
		case 6:
			result = new SshapeBrick();
			break;
		}

		return result;
	}

	private void Rotation() {
		Board.CanRotationResult res = m_Board.canRotation(m_Brick,
				AbsBrick.CLOCKWISE);
		if (m_Brick != null && res.canRotation) {
			m_SoundRotation.play();
			if (res.offsetLeft > 0) {
				m_Brick.setCoordColumn(m_Brick.getCoordColumn()
						+ res.offsetLeft);
				m_Brick.setX(m_Board.getX() + (m_Brick.getCoordColumn() - 1)
						* m_Board.getPiceWidth());
			}
			if (res.offsetRight > 0) {
				m_Brick.setCoordColumn(m_Brick.getCoordColumn()
						- res.offsetRight);
				m_Brick.setX(m_Board.getX() + (m_Brick.getCoordColumn() - 1)
						* m_Board.getPiceWidth());

			}
			m_Brick.Rotation(AbsBrick.CLOCKWISE);
		}
	}

	private void MoveLeft() {
		if (m_Brick != null && m_Board.canMoveLeft(m_Brick)) {
			m_SoundButton.play();
			m_Brick.setCoordColumn(m_Brick.getCoordColumn() - 1);
			m_Brick.setX(m_Board.getX() + (m_Brick.getCoordColumn() - 1)
					* m_Board.getPiceWidth());
		}
	}

	private void MoveRight() {
		if (m_Brick != null && m_Board.canMoveRight(m_Brick)) {
			m_SoundButton.play();
			m_Brick.setCoordColumn(m_Brick.getCoordColumn() + 1);
			m_Brick.setX(m_Board.getX() + (m_Brick.getCoordColumn() - 1)
					* m_Board.getPiceWidth());
		}
	}

	private void Down() {
		if (m_GameSpeed == ACC_SPEED)
			m_SoundButton.play();

		m_Brick.setCoordLine(m_Brick.getCoordLine() + 1);
		m_Brick.setY(m_Board.getY() + ((Board.LINES_COUNT - 1) - TranslateY())
				* m_Board.getPiceHeight());
	}

	/*
	 * 将方块的左上角座标转换成用于绘图的左下角座标
	 */
	private int TranslateY() {
		return m_Brick.getCoordLine() + 3;
	}

	public void StartNewGame() {

		m_Score = 0;

		m_IsGameOver = false;
		m_Pause = false;
		m_HorizMoveDirect = H_STATIC;
		m_GameSpeed = DEFAULT_SPEED;

		m_Board.clearAll();

		// m_DelterTime = 0;
		if (m_NextBrick != null) {
			m_NextBrick.remove();
			m_NextBrick = null;
		}
		if (m_Brick != null) {
			m_Brick.remove();
			m_Brick = null;
		}

		m_Brick = makeNewTetris();

		m_NextBrick = makeNewTetris();
		initBricks(0, 4);
		m_Stage.addActor(m_Brick);
		m_Stage.addActor(m_NextBrick);
		// m_Music=Audio.newMusic();

		// if (m_GameThread == null || !m_GameThread.isAlive()) {
		// m_GameThread = new GameThread();
		// m_GameThread.start();
		// }
		m_MainTimer.start();
		m_Music.setLooping(true);
		m_Music.play();
	}

	private void ShowNextBrick() {
		m_Board.copyTetris(m_Brick);

		int c = m_Board.clearLines();

		if (c > 0) {
			m_SoundDelete[c - 1].play();
			updateScore(c);
		}
		m_Brick.remove();
		m_Brick.dispose();
		m_Brick = null;

		m_Brick = m_NextBrick;

		m_NextBrick = makeNewTetris();
		initBricks(0, 4);

		// m_Stage.addActor(m_Tetris);
		m_Stage.addActor(m_NextBrick);
	}

	/**
	 * 根据消除的行数计算得分
	 * 
	 * @param count
	 */
	private void updateScore(int count) {
		switch (count) {
		case 1:
			m_Score += 100;
			break;
		case 2:
			m_Score += 300;
			break;
		case 3:
			m_Score += 500;
			break;
		case 4:
			m_Score += 1000;
			break;
		default:
			m_Score += 0;
		}
		// m_LabelScore.setText(m_context.getString(R.string.game_score,
		// m_Score));
		// Log.i(this.getClass().getSimpleName(), "Score="+m_Score);
	}

	/*
	 * 初始方块的大小和位置
	 */
	private void initBricks(int line, int column) {
		float picew = m_Board.getPiceWidth();
		float piceh = m_Board.getPiceHeight();
		m_Brick.setPiceSize(picew, piceh);
		m_Brick.setCoordLine(line);
		m_Brick.setCoordColumn(column);

		m_Brick.setX(m_Board.getX() + (column - 1) * picew);
		m_Brick.setY(m_Board.getY() + ((Board.LINES_COUNT - 1) - TranslateY())
				* piceh);

		m_NextBrick.setPiceSize(picew * 0.8f, piceh * 0.8f);
		m_NextBrick.setCoordLine(line);
		m_NextBrick.setCoordColumn(column);
		m_NextBrick.setX(m_Board.getX() + m_Board.getWidth() + 60f * POWER_X);
		m_NextBrick.setY(520f * POWER_Y + m_Board.getY());
	}

	// private void Playing() {
	// if (m_IsGameOver)
	// return;
	// if (m_Board.canDown(m_Tetris)) {
	// Down();
	// } else {
	// if (m_Tetris.getCoordLine() == 0) {
	// m_IsGameOver = true;
	// return;
	// }
	// m_Board.copyTetris(m_Tetris);
	// updateScore(m_Board.clearLines());
	// m_Tetris.remove();
	// m_Tetris.dispose();
	// m_Tetris = null;
	// ShowNextBrick();
	// }
	//
	// }

	private void CreateDialog() {
		Window.WindowStyle style = new Window.WindowStyle();

		style.titleFont = m_Font;
		style.background = new TextureRegionDrawable(new TextureRegion(
				new Texture(
						Gdx.files.internal("background/gameover269x377.png")),
				0, 0, 269, 377));

		m_GameOverdlg = new GameOverDialog("", style, m_Font);
		// dlg.show(m_Stage);
	}

	private void SaveState() {
		// m_Tetris,m_NextTetris,m_Score,m_Board
		if (this.m_IsGameOver)
			return;
		// Bundle bundle = new Bundle();
		SharedPreferences shp = ((MainActivity) m_Context)
				.getSharedPreferences("state", Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = shp.edit();
		editor.putString("current", m_Brick.getClass().getName());
		editor.putInt("currentColumn", m_Brick.getCoordColumn());
		editor.putInt("currentLine", m_Brick.getCoordLine());
		editor.putInt("currentState", m_Brick.getCurrentState());
		editor.putString("next", m_NextBrick.getClass().getName());
		editor.putInt("score", m_Score);
		// int[] boarddata = m_Board.getBoardData();
		// for (int i = 0; i < boarddata.length; i++)
		editor.putString("board", m_Board.getBoardData());
		editor.commit();

		// bundle.putString("current", m_Brick.getClass().getName());
		// bundle.putInt("currentColumn", m_Brick.getCoordColumn());
		// bundle.putInt("currentLine", m_Brick.getCoordLine());
		// bundle.putString("next", m_NextBrick.getClass().getName());
		// bundle.putInt("score", m_Score);
		// bundle.putIntArray("board", m_Board.getBoardData());

	}

	private void LoadState() {
		SharedPreferences shp = ((MainActivity) m_Context)
				.getSharedPreferences("state", Activity.MODE_PRIVATE);

		Class<?> cla;
		try {
			cla = (Class<?>) Class.forName(shp.getString("current", ""));
			m_Brick = (AbsBrick) cla.newInstance();

			cla = Class.forName(shp.getString("next", ""));
			m_NextBrick = (AbsBrick) cla.newInstance();

			m_Brick.setCurrentState(shp.getInt("currentState", 0));

			initBricks(shp.getInt("currentLine", 0),
					shp.getInt("currentColumn", 0));
//			int i = 0;
//			int[] board;
//
//			board = new int[m_Board.getBoardDataCount()];
//			for (i = 0; i < m_Board.getBoardDataCount(); i++)
//				board[i] = shp.getInt("board" + i, 0);

			m_Board.setBoardData(shp.getString("board","0" ));
			m_Score = shp.getInt("score", 0);
			m_Stage.addActor(m_Brick);
			m_Stage.addActor(m_NextBrick);
			m_GameSpeed = DEFAULT_SPEED;

			// m_Music=Audio.newMusic();

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void initSounds() {
		m_Music = Gdx.audio.newMusic(Gdx.files.internal("music/gameBg.ogg"));
		m_SoundButton = Gdx.audio.newSound(Gdx.files
				.internal("sounds/button.ogg"));
		m_SoundRotation = Gdx.audio.newSound(Gdx.files
				.internal("sounds/rotation.ogg"));
		m_SoundDown = Gdx.audio.newSound(Gdx.files.internal("sounds/down.ogg"));
		m_SoundLose = Gdx.audio.newSound(Gdx.files.internal("sounds/lost.ogg"));
		m_SoundDelete = new Sound[4];
		for (int i = 0; i < 4; i++)
			m_SoundDelete[i] = Gdx.audio
					.newSound(Gdx.files.internal("sounds/delete"
							+ String.valueOf(i + 1) + ".ogg"));

	}

	private void ResumeGame() {
		LoadState();
		m_IsGameOver = false;
		m_Pause = false;
		m_HorizMoveDirect = H_STATIC;
		m_GameSpeed = DEFAULT_SPEED;
		m_MainTimer.start();
		m_Music.setLooping(true);
		m_Music.play();

	}

	public void SetGameState(boolean isnew) {
		this.m_NewGame = isnew;
	}
}
