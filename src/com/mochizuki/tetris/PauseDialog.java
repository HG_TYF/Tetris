package com.mochizuki.tetris;

import android.content.Context;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class PauseDialog extends Dialog {

	public interface GameControl {
		public void NewGame();

		public void Resume();

		public void Exit();

	}

	private float POWER_X, POWER_Y;
	private float SCREEN_WIDTH, SCREEN_HEIGHT;

	private TextButton m_btnNewGame;
	private TextButton m_btnExit;
	private TextButton m_btnResume;

	private Context m_Context;

	private BitmapFont m_Font;

	private GameControl m_gameControl;
	private Texture m_buttonTexture;

	public PauseDialog(String title, WindowStyle style, Context context,
			GameControl ctrl) {
		super(title, style);
		// TODO Auto-generated constructor stub
		m_gameControl = ctrl;

		SCREEN_WIDTH = Gdx.graphics.getWidth();
		SCREEN_HEIGHT = Gdx.graphics.getHeight();
		POWER_X = SCREEN_WIDTH / 640f;
		POWER_Y = SCREEN_HEIGHT / 960f;

		m_Context = context;

		m_Font = new BitmapFont(Gdx.files.internal("fonts/chnfont.fnt"), false);
		m_Font.setScale(POWER_X);

		// 按钮外观
		TextureRegionDrawable mDrawableBtnUp;
		TextureRegionDrawable mDrawableBtnDown;
		m_buttonTexture = new Texture(Gdx.files.internal("buttons/button228x31.png"));
		mDrawableBtnUp = new TextureRegionDrawable(new TextureRegion(
				m_buttonTexture, 0, 0, 107, 31));
		mDrawableBtnDown = new TextureRegionDrawable(new TextureRegion(
				m_buttonTexture, 121, 0, 107, 31));

		TextButton.TextButtonStyle mBtnStyle = new TextButton.TextButtonStyle(
				mDrawableBtnUp, mDrawableBtnDown, mDrawableBtnUp);
		mBtnStyle.font = m_Font;

		// 创建按钮
		m_btnNewGame = new TextButton(
				m_Context.getString(R.string.btn_new_game), mBtnStyle);
		m_btnExit = new TextButton(m_Context.getString(R.string.btn_exit),
				mBtnStyle);
		m_btnResume = new TextButton(m_Context.getString(R.string.btn_resume),
				mBtnStyle);

		m_btnNewGame.setWidth(256f * POWER_X);
		m_btnNewGame.setHeight(64f * POWER_Y);
		m_btnNewGame.setX((getWidth() - m_btnNewGame.getWidth()) / 2);
		m_btnNewGame.setY(84f * POWER_Y);

		m_btnExit.setWidth(m_btnNewGame.getWidth());
		m_btnExit.setHeight(m_btnNewGame.getHeight());
		m_btnExit.setX(m_btnNewGame.getX());
		m_btnExit.setY(20f * POWER_Y);

		m_btnResume.setWidth(m_btnNewGame.getWidth());
		m_btnResume.setHeight(m_btnNewGame.getHeight());
		m_btnResume.setX(m_btnNewGame.getX());
		m_btnResume.setY(148f * POWER_Y);

		m_btnNewGame.addListener(cllistener);
		m_btnExit.addListener(cllistener);
		m_btnResume.addListener(cllistener);

		this.addActor(m_btnExit);
		this.addActor(m_btnNewGame);
		this.addActor(m_btnResume);

	}


	// 设置按钮监听程序
	private ClickListener cllistener = new ClickListener() {

		@Override
		public boolean touchDown(InputEvent event, float x, float y,
				int pointer, int button) {
			// TODO Auto-generated method stub
			return super.touchDown(event, x, y, pointer, button);
		}

		@Override
		public void touchUp(InputEvent arg0, float arg1, float arg2, int arg3,
				int arg4) {
			// TODO Auto-generated method stub
			if (arg0.getListenerActor() == m_btnNewGame)
				m_gameControl.NewGame();
			if (arg0.getListenerActor() == m_btnResume)
				m_gameControl.Resume();
			if (arg0.getListenerActor() == m_btnExit)
				m_gameControl.Exit();
			super.touchUp(arg0, arg1, arg2, arg3, arg4);
		}
	};

	@Override
	public void setWidth(float width) {
		// TODO Auto-generated method stub
		super.setWidth(width);
		if (m_btnNewGame != null)
			m_btnNewGame.setX(getX() + (getWidth() - m_btnNewGame.getWidth())
					/ 2);
		if (m_btnExit != null)
			m_btnExit.setX(m_btnNewGame.getX());
		if (m_btnResume != null)
			m_btnResume.setX(m_btnNewGame.getX());
	}



}
